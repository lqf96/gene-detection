# Gene Detection Experiment
Gene detection experiment for Cloud Data Management II using Frequent Pattern Growth.
See [report](HW2-Report.md) for more details.

## License
[GNU GPLv3](LICENSE)
