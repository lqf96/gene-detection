let _ = require("lodash");

//Find indexes of array items that meets certain condition
exports.find_indices = function find_indices(array, predicate=_.identity) {
    let result = [];

    for (let i=0;i<array.length;i++)
        if (predicate(array[i], i))
            result.push(i);

    return result;
}

//Calculate matrix sum by column
exports.matrix_col_sum = function matrix_col_sum(matrix) {
    let size_0 = matrix.length;
    let size_1 = matrix[0].length;

    let result = [];
    for (let i=0;i<size_1;i++) {
        let col_sum = 0;
        for (let j=0;j<size_0;j++)
            col_sum += matrix[j][i];
        result.push(col_sum);
    }

    return result;
}
