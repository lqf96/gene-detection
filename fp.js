class Node {
    //Constructor
    constructor(freq=0, id=null) {
        //Frequency
        this.freq = freq;
        //Next
        this.next = null;
        //Children
        this.children = new Map();
        //Parent
        this.parent = null;
        //ID
        this.id = id;
    }

    //Add a child node
    add(id) {
        let child = this.children.get(id);
        let new_node = false;

        if (!child) {
            child = new Node(1, id);
            child.parent = this;

            new_node = true;
            this.children.set(id, child);
        } else {
            child.freq++;
        }

        return [new_node, child];
    }

    //Print frequency pattern tree
    print_tree(log_func=console.info, n_indent=0) {
        if (n_indent==0)
            log_func("[print_tree] Printing tree:");

        for (let [id, node] of this.children) {
            //Indentation
            let indentation = "";
            for (let i=0;i<n_indent;i++)
                indentation += "  ";
            //Formatted node
            log_func(`${indentation}${id}: ${node.freq}`);

            //Print children
            node.print_tree(log_func, n_indent+1);
        }
    }

    //Collect prefix and frequency
    collect_prefix_freq(col) {
        let node = this.parent;

        while (node.parent) {
            //Update frequency
            let freq = col.get(node.id)||0;
            freq += this.freq;
            col.set(node.id, freq);

            node = node.parent;
        }
    }
}

//Collect conditional frequent pattern tree
function collect_cfpt(ext_header_table, log_func=console.info) {
    //Threshold
    const OCCUR_THRESHOLD = 8500;
    //Total threshold
    const TOTAL_THRESHOLD = 10000;

    let cfpt = new Map();
    let flag = false;

    for (let [id, node] of ext_header_table) {
        log_func(`[collect_cfpt] Collecting ${id}...`);
        flag = false;

        let col = new Map();
        let p = node.next;
        let freq = 0;

        while (p) {
            //Top-level node or too few occurance
            if ((!p.parent.parent)||(p.freq<OCCUR_THRESHOLD)) {
                p = p.next;
                continue;
            }

            //Collect prefix and frequency
            p.collect_prefix_freq(col);
            //Update frequency
            freq += p.freq;

            p = p.next;
        }

        if (freq>=TOTAL_THRESHOLD) {
            //Remove items with frequency below total threshold
            for (let [col_id, col_id_freq] of col)
                if (col_id_freq<TOTAL_THRESHOLD)
                    col.delete(col_id);

            //Only add non-empty result
            if (col.size>0)
                cfpt.set(id, [freq, col]);
        }
    }

    return cfpt;
}

//Build frequent pattern tree
exports.build_fpt = function build_fpt(data, header_table, log_func=console.info) {
    //Tree root node
    let fp_tree_root = new Node();

    //Extended header table
    let ext_header_table = new Map();
    for (let [id, freq] of header_table)
        ext_header_table.set(id, new Node(freq, id));
    log_func("[build_fpt] Extended header table built");
    //Current header
    let cur_header = new Map(ext_header_table);

    //Add data to FP tree
    for (let row of data) {
        let node = fp_tree_root;

        for (let id of row) {
            let [new_node, child_node] = node.add(id);
            if (new_node) {
                cur_header.get(id).next = child_node;
                cur_header.set(id, child_node);
            }
            node = child_node;
        }
    }

    //Collect CFP tree
    log_func("[build_fpt] Collecting CFP tree...");
    let cfpt = collect_cfpt(ext_header_table)

    return [fp_tree_root, ext_header_table, cfpt];
}

//Test data
exports.test_data = [
    ["f", "c", "a", "m", "p"],
    ["f", "c", "a", "b", "m"],
    ["f", "b"],
    ["c", "b", "p"],
    ["f", "c", "a", "m", "p"]
];

//Test header table
exports.test_header_table = new Map([
    ["f", 4],
    ["c", 4],
    ["a", 3],
    ["b", 3],
    ["m", 3],
    ["p", 3]
]);
