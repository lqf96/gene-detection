# 云数据管理 第二次大作业 实验报告
鲁其璠 - 2014013423 - lqf.1996121@gmail.com

## 实验环境
* 笔记本型号：MacBook Pro (Retina, 13-inch, Early 2015)
* 操作系统：macOS High Sierra 10.13.2
* 中央处理器：3.1 GHz Intel Core i7
* 内存：16GB DDR3
* 显卡：Intel集成显卡

## 实验目标
本次实验目标为寻找能够代表患病人群的致病基因，搜寻的原始数据为20000个人（其中10000人患病，另10000人健康）的1号染色体的数十万个SNP，最终期望能够能够找到不多于10个SNP基因的SNP组合。

## 方法简述
本次实验中，先后采用了两种方法挖掘可能的致病基因，最终采用的是基于Frequent Pattern的方法，以Node.js实现。

### Frequent Pattern搜索
Frequent Pattern搜索的想法来源于课件第五讲，具体思路在下面的实验报告中有具体描述。由于SNP数量巨大，因此直接对全部基因进行Frequent Pattern分析并不可行，需要首先减小搜索范围再进行挖掘。Frequent Pattern搜索的优势在于可以快速利用已有数据挖掘频繁出现的基因模式，缺点则是需要调节参数以便控制发掘模式的数量。

### 特征选择方法
本次大作业中的问题也可以采用另一种方法加以描述，即将数十万个SNP看成数十万个特征，然后使用特征选择方法从这数十万个特征中选择出不超过十个特征。在尝试这一过程时，我选择了`sklearn`库中有关特征选择的一系列工具，即`SelectKBest`和`RFE`选取最佳特征，并以`chi2`（卡方统计量）、`f_classif`等为指标进行分析。不过这一条路并没有走通，问题在于`sklearn`的特征选择几乎都基于特征量是连续量的假设，而对离散变量几乎没有任何支持，另外，`sklearn`在处理大量数据时，很容易出现内存不足的情况。种种原因使得我最终放弃了特征选择方法。

## 数据格式
本次实验的目标数据集包含了如下文件：
* `chr1.legend`：包含了1号染色体上所有基因的相关数据，其表头格式为：

```
rs position a0 a1
```

分别是这个基因的唯一标识、在染色体上的位置与碱基信息，在本次作业中只有基因的下标与它的唯一标识是我们关心的，因而本次作业中我们选择逐行读入文件，然后以空字符分割每行，取第一列`rs`的值即可。

```js
//Legend
let legend = fs.readFileSync(path.join(dataset_dir, `chr${legend_id}.legend`), "utf8")
    .split("\n")
    .map((line) => line.split(whitespace_rx)[0])
    .shift();
log_func("[load_dataset] Legend loaded.")
```

* `filteredSnps.5kbp.simpleFormat`：包含了更可能出现致病基因的基因列表，该文件缩小了基因查找范围，它的格式为：

```
SNP_ID Pos
```

分别是这个基因的唯一标识与在染色体上的位置，与之前的数据类似，我们也对文件分行处理，然后取第一列`SNP_ID`即可。

```js
//Filtered SNPs
let filtered_snps = fs.readFileSync(path.join(dataset_dir, "filteredSnps.5kbp.simpleFormat"), "utf8")
    .split("\n")
    .map((line) => line.split(whitespace_rx)[0])
    .shift();
log_func("[load_dataset] Filtered SNPs loaded.")
```

* `matrix.gz`：包含了所有样本的所有基因数据。每一行为所有人的某一个基因的携带情况，分别代表双显性基因、单显性基因、双隐性基因和数据缺失。为了减少读入数据的量，我们先利用`chr1.legend`和`filteredSnps.5kbp.simpleFormat`两个文件缩小致病基因的范围，然后根据缩小的范围选择性地读入基因数据。

```js
```

* `pheno`：包含了所有样本患病与否的情况。患病为1，不患病为0。读取时仅需转换数据格式，不需要作特别处理，不过同样为了节省内存空间，我们将其转换为了`Uint8Array`格式。

```js
//Pheno
let pheno = fs.readFileSync(path.join(dataset_dir, "pheno"), "utf8")
    .split("\n")
    .map(Number);
pheno = new Uint8Array(pheno);
log_func(`[load_dataset] Pheno loaded: ${pheno.shape[0]}`);
```

## 使用方法
在使用本次作业的命令行工具以前，需要先将`matrix.gz`解压缩：

```sh
gunzip matrix.gz
```

确保目标机器安装了`node`和`npm`，推荐`node`主版本为9，在工程目录下执行`npm install`安装所需依赖，然后通过`index.js`运行作业。

`index.js`接受如下命令行参数：

* `-d`（或者`--dataset-dir`）：数据集路径。
* `-l`（或者`--legend-id`）：文件`chr*.legend`星号部分的值，对于数据集三取1。

## Frequent Pattern算法
相比于其它算法，Frequent Pattern树对二值特征进行了压缩，因而更加节约内存。另外FP算法可以保证仅需遍历两次数据集就完成对于频繁模式的查找，因而亦具有较高的效率。

本次作业中FP算法分别实现在`index.js`和`fp.js`中，另有`utils.js`提供前两者需要的部分实用函数。

### 构建FP树
按照课件中描述的算法构建FP树：

* 首先扫描数据，查找单项模式频率以便建立表头。
* 对单项模式根据出现频率降序排序。
* 再次扫描数据库，构建FP树。

如下的代码实现了FP树的节点对象，其中的成员方法涵盖了节点的添加、树的递归输出以及前缀的递归生成：

```js
class Node {
    //Constructor
    constructor(freq=0, id=null) {
        //Frequency
        this.freq = freq;
        //Next
        this.next = null;
        //Children
        this.children = new Map();
        //Parent
        this.parent = null;
        //ID
        this.id = null
    }

    //Add a child node
    add(id) {
        let child = this.children.get(id);
        let new_node = false;

        if (!child) {
            child = new Node(1, id);
            child.parent = this;

            new_node = true;
            this.children.set(id, child);
        } else {
            child.freq++;
        }

        return [new_node, child];
    }

    //Print frequency pattern tree
    print_tree(log_func=console.info, n_indent=0) {
        log_func("Printing tree:");

        for (let [id, node] of this.children) {
            //Indentation
            let indentation = "";
            for (let i=0;i<n_indent;i++)
                indentation += "  ";
            //Formatted node
            log_func(`[print_tree] ${indentation}${id}: ${node.freq}`);

            //Print children
            node.print_tree(log_func, n_indent+1);
        }
    }

    //Collect prefix and frequency
    collect_prefix_freq(col) {
        let node = this.parent;

        while (node.parent) {
            //Update frequency
            let freq = col.get(node.id)||0;
            freq += this.freq;
            col.set(node.id, freq);

            node = node.parent;
        }
    }
}
```

如下的代码建立了一颗FP树并将数据插入到树中：

```js
//Tree root node
let fp_tree_root = new Node();

//...

//Add data to FP tree
for (let row of data) {
    let node = fp_tree_root;

    for (let id of row) {
        let [new_node, child_node] = node.add(id);
        if (new_node) {
            cur_header.get(id).next = child_node;
            cur_header.set(id, child_node);
        }
        node = child_node;
    }
}
```

### 模式挖掘
当我们有了FP树之后，就可以着手挖掘高频模式。
* 用FP树构建挖掘条件模式基。
* 通过条件模式基构建条件FP树。
* 最后利用条件FP树生成高频模式，得到可能的致病基因组合。

相应的代码如下：

```js
//Collect conditional frequent pattern tree
function collect_cfpt(ext_header_table, log_func=console.info) {
    //Threshold
    const OCCUR_THRESHOLD = 10000;
    //Total threshold
    const TOTAL_THRESHOLD = 3;

    let cfpt = new Map();
    let flag = false;

    for (let [id, node] of ext_header_table) {
        log_func(`[collect_cfpt] Collecting ${id}...`);
        flag = false;

        let col = new Map();
        let p = node.next;
        let freq = 0;

        while (p) {
            //Top-level node or too few occurance
            if ((!p.parent.parent)||(p.freq<OCCUR_THRESHOLD)) {
                p = p.next;
                continue;
            }

            //Collect prefix and frequency
            p.collect_prefix_freq(col);
            //Update frequency
            freq += p.freq;

            p = p.next;
        }

        if (freq>=TOTAL_THRESHOLD) {
            //Remove items with frequency below total threshold
            for (let [col_id, col_id_freq] of col)
                if (col_id_freq<TOTAL_THRESHOLD)
                    col.delete(col_id);

            //Only add non-empty result
            if (col.size>0)
                cfpt.set(id, [freq, col]);
        }
    }

    return cfpt;
}
```

### 优化
#### SNP筛选
为了最大限度减轻搜寻工作量，我们需要对SNP进行筛选。显然，如果某一SNP满足如下条件，那么它不太可能成为致病基因之一：

* 该SNP经常出现在全体人群中。
* 该SNP很少出现在得病人群中。
* 该SNP同时有很大概率出现在健康人群与得病人群中。

针对以上三种情况，对SNP实施筛选：

```js
//Sick indices
let sick_indexes = find_indices(y, (y) => y==1);
//Sick matrix
let x_sick = _.at(x, sick_indexes);

//Convert matrix to binary data
x = x.map((row) => row.map((data) => data<2));
x_sick = x_sick.map((row) => row.map((data) => data<2));

//Frequency
let freq = matrix_col_sum(x);
let freq_sick = matrix_col_sum(x_sick);
//Threshold
let threshold = PERCENT/100*x.length;
let threshold_sick = PERCENT_SICK/100*x_sick.length;
//Print threshold
log_func(`[predict_genes] Threshold: ${threshold}`);
log_func(`[predict_genes] Threshold (Sick): ${threshold_sick}`);

//Useful SNP indices
let useful = _.zip(freq, freq_sick)
    .map(([freq, freq_sick]) => (freq<threshold)&&(freq_sick>=threshold_sick));
let useful_indices = find_indices(useful);
log_func(`[predict_genes] Number of useful genes: ${useful_indices.length}`);
```

#### 条件FP树构建剪枝
对合并过程中，顶级节点和频率过小的情况剪枝，以提高运行效率：

```js
if ((!p.parent.parent)||(p.freq<OCCUR_THRESHOLD)) {
    p = p.next;
    continue;
}
```

## 运行结果
* SNP筛选一节中提到了三个筛选不太可能是致病基因的条件，实际运行中，取阈值如下：
  - 全体人群中，某一基因的出现频率不超过85%。
  - 得病人群中，某一基因的出现频率不少于80%。
* 在取以上阈值时，运行结果为：

```
Map {
  'rs1043657' => [ 13884, Map { 'rs1013579' => 13884 } ],
  'rs10489455' => [ 11320, Map { 'rs1043657' => 11320, 'rs1013579' => 11320 } ]
}
```

因此可以取以下几组值作为预测结果：

```
rs1043657 rs1013579
rs10489455 rs1043657 rs1013579
rs10489455 rs1043657
rs10489455 rs1013579
```

## 感想总结
这次大作业中我最深刻的感悟在于，对于机器学习，不能够将算法本身当作黑盒，而是应该针对具体的问题，采用合适的算法，并且充分了解所采用算法的原理，方能真正解决问题。另外本次大作业没有一个明确的思路，而是鼓励大家通过不同方法解决问题，可以说是很好的尝试，只是我一直未能找到很好的替代方法解决本次作业，也可以说是相当遗憾。
