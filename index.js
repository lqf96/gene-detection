#! /usr/bin/env node --max-old-space-size=15360
let fs = require("fs");
let path = require("path");
let zlib = require("zlib");

let _ = require("lodash");
let {ArgumentParser} = require("argparse");
let lineByLine = require("n-readlines");

let {build_fpt} = require("./fp");
let {find_indices, matrix_col_sum} = require("./utils");

//Load dataset from given position
let load_dataset = exports.load_dataset = function load_dataset(dataset_dir, legend_id, log_func=console.info) {
    //Zero ASCII code
    const ZERO_CODE = 48;
    //Whitespace regular expression
    const WHITESPACE_RX = /[ \t]+/;
    //Show every N lines
    const SHOW_EVERY_N_LINES = 10000;

    //Pheno
    let pheno = fs.readFileSync(path.join(dataset_dir, "pheno"), "utf8")
        .split("\n")
        .map(Number);
    pheno.pop();
    pheno = new Uint8Array(pheno);
    log_func(`[load_dataset] Pheno loaded: ${pheno.length}`);

    //Legend
    let legend = fs.readFileSync(path.join(dataset_dir, `chr${legend_id}.legend`), "utf8")
        .split("\n")
        .map((line) => line.split(WHITESPACE_RX)[0]);
    legend.shift();
    log_func(`[load_dataset] Legend loaded: ${legend.length}`);

    //Filtered SNPs
    let filtered_snps = fs.readFileSync(path.join(dataset_dir, "filteredSnps.5kbp.simpleFormat"), "utf8")
        .split("\n")
        .map((line) => line.split(WHITESPACE_RX)[0]);
    filtered_snps.shift();
    log_func(`[load_dataset] Filtered SNPs loaded: ${filtered_snps.length}`);

    //Get filtered SNPs indices
    let filtered_snps_indices = filtered_snps.map((snp) => legend.indexOf(snp));
    //filtered_snps_indices.sort();
    log_func("[load_dataset] Filtered SNPs indices calculated.");

    //Data matrix
    let matrix = [];
    for (let i=0;i<pheno.length;i++)
        matrix.push(new Uint8Array(filtered_snps.length));

    let matrix_liner = new lineByLine(path.join(dataset_dir, "matrix"));
    let line_no = 0;
    let useful_line_counter = 0;
    //Build matrix
    while (true) {
        if (line_no%SHOW_EVERY_N_LINES==0)
            log_func(`[load_dataset] ${line_no} lines processed; ${useful_line_counter} useful.`);

        //Read new line
        let line = matrix_liner.next();
        if (!line)
            break;
        line_no++;

        let snp_index_index = filtered_snps_indices.indexOf(line_no);
        //Ignore line
        if (snp_index_index==-1)
            continue;
        useful_line_counter++;

        for (let i=0;i<pheno.length;i++)
            matrix[i][snp_index_index] = line[i]-ZERO_CODE;
    }
    log_func(`[load_dataset] Matrix loaded: (${matrix.length}, ${matrix[0].length})`);

    return [matrix, pheno, filtered_snps];
}

//Predict genes
let predict_genes = exports.predict_genes = function predict_genes(dataset_dir, legend_id, log_func=console.info) {
    const PERCENT = 85;
    const PERCENT_SICK = 80;

    //Load dataset into memory
    let [x, y, filtered_snps] = load_dataset(dataset_dir, legend_id, log_func);

    //Sick indices
    let sick_indexes = find_indices(y, (y) => y==1);
    //Sick matrix
    let x_sick = _.at(x, sick_indexes);
    log_func(`[predict_genes] Sick amount: ${x_sick.length}`);

    //Convert matrix to binary data
    x = x.map((row) => row.map((data) => data<2));
    x_sick = x_sick.map((row) => row.map((data) => data<2));

    //Frequency
    let freq = matrix_col_sum(x);
    let freq_sick = matrix_col_sum(x_sick);
    //Threshold
    let threshold = PERCENT/100*x.length;
    let threshold_sick = PERCENT_SICK/100*x_sick.length;
    //Print threshold
    log_func(`[predict_genes] Threshold: ${threshold}`);
    log_func(`[predict_genes] Threshold (Sick): ${threshold_sick}`);

    //Useful SNP indices
    let useful = _.zip(freq, freq_sick)
        .map(([freq, freq_sick]) => (freq<threshold)&&(freq_sick>=threshold_sick));
    let useful_indices = find_indices(useful);
    log_func(`[predict_genes] Number of useful genes: ${useful_indices.length}`);

    //Header table
    let header_table = new Map();
    for (snp_index of useful_indices)
        header_table.set(snp_index, freq_sick[snp_index]);
    log_func(`[predict_genes] Header table generated.`)
    //Data
    let data = x.map((row) => find_indices(
        _.zip(row, useful).map((([x, snp_useful]) => x&&snp_useful))
    ));
    log_func(`[predict_genes] Data generated.`);

    //Calculate CFPT
    let [_1, _2, cfpt] = build_fpt(data, header_table, log_func);
    //Result
    let result = new Map();
    for (let [id, [freq, col]] of cfpt) {
        let col_new = new Map();
        for (let [col_id, col_id_freq] of col)
            col_new.set(filtered_snps[col_id], col_id_freq);
        result.set(filtered_snps[id], [freq, col_new]);
    }

    return result;
}

//Command line entry
if (require.main===module) {
    let parser = new ArgumentParser({
        addHelp: true,
        description: "Gene detection demo program."
    });

    //Arguments
    parser.addArgument(["-d", "--dataset-dir"], {
        required: true,
        help: "Dataset directory"
    });
    parser.addArgument(["-l", "--legend-id"], {
        required: true,
        help: "Legend ID",
        type: "int"
    });

    //Parse arguments and run program
    let args = parser.parseArgs();
    let result = predict_genes(args.dataset_dir, args.legend_id);

    //Show result
    console.info(`[__main__] Prediction result:`);
    console.info(result);
}
